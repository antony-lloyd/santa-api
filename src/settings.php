<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header,
        'determineRouteBeforeAppMiddleware' => true,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

		// Database settings
		'db' => [
		    'driver' => 'mysql',
			'host'	=> '127.0.0.1',
			'database'	=> 'santa',
			'username'	=> 'santa',
			'password'	=> 'Abc123',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => ''
		],

        'JWT_KEY' => 'a4iwekSE3q7reX68SdfYCb6qz8GaneEcTuks'
    ],
];
