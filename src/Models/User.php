<?php
namespace Santa\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $hidden = [ 'password' ];

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function address()
    {
        return $this->hasOne(UserAddress::class);
    }
}