<?php
namespace Santa\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }
}