<?php
namespace Santa\Models;

use Illuminate\Database\Eloquent\Model;

class EntryMessage extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function entry()
    {
        return $this->belongsTo(Entry::class);
    }

    public function exportForUi($fetchingUser, $useUsersName = false)
    {
        $person = $this->user_id === $fetchingUser ? 'You' : ($useUsersName ? $this->user->name : 'Santa');
        return [
            'id' => $this->id,
            'person' => $person,
            'message' => $this->message,
            'date' => date('d/m/Y H:i:s', strtotime($this->created_at))
        ];
    }
}