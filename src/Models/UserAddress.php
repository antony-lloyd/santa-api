<?php
namespace Santa\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $hidden = [ 'password' ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}