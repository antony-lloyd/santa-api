<?php

namespace Santa\Services;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use \Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Guzzle\Common\Exception\UnexpectedValueException;

/**
 * JSON Web Token class
 * @package Api\Auth
 */
class Token
{
    private $container;

    const VERIFY_ERROR_INVALID = -1;
    const VERIFY_ERROR_SIGNATURE = -2;
    const VERIFY_ERROR_BEFORE = -3;
    const VERIFY_ERROR_EXPIRED = -4;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Generates a JWT
     * @param array $data Data to provide as a payload
     * @return string The JWT
     */
    public function generate(array $data)
    {
        // Token expires in a hour after time issued
        $payload = [
            'exp' => strtotime('+5 hour'),
            'iat' => time()
        ];

        $payload = array_merge($payload, $data);

        // Generate the JWT
        return JWT::encode($payload, $this->container->settings['JWT_KEY']);
    }

    /**
     * Verifies a JWT
     * @param $token The JWT to verify
     * @return mixed Decoded payload data on success, error code on failure
     */
    public function verify($token)
    {
        // Try to decode the JWT, and catch any exceptions
        try {
            return JWT::decode($token, $this->container->settings['JWT_KEY'], ['HS256']);
        } catch (UnexpectedValueException $e) {
            return self::VERIFY_ERROR_INVALID;
        } catch (SignatureInvalidException $e) {
            return self::VERIFY_ERROR_SIGNATURE;
        } catch (BeforeValidException $e) {
            return self::VERIFY_ERROR_BEFORE;
        } catch (ExpiredException $e) {
            return self::VERIFY_ERROR_EXPIRED;
        }
    }

    /**
     * Returns a string of the verify response
     * @param $response The result of the verify
     * @return string The formated result
     */
    public function formatVerifyResponse($response)
    {
        switch ($response) {
            case self::VERIFY_ERROR_INVALID:
                return 'Invalid token';
            case self::VERIFY_ERROR_SIGNATURE:
                return 'Invalid token signature';
            case self::VERIFY_ERROR_BEFORE:
                return 'Token not ready';
            case self::VERIFY_ERROR_EXPIRED:
                return 'Token expired';
            default:
                return 'Token valid';
        }
    }
}