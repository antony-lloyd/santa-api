<?php

use Santa\Controllers\Group\GroupController;
use Santa\Controllers\Group\GroupPeopleController;
use Santa\Controllers\Entry\MessageController;
use Santa\Controllers\User\UserController;
use Slim\Http\Request;
use Slim\Http\Response;
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') die();
// Routes
$app->group('/group', function() {

    $this->post('', GroupController::class . ':store')->setArgument('access', 'public');
    $this->post('/join', GroupController::class . ':join')->setArgument('access', 'public');

    $this->group('/{groupId}', function() {

        $this->get('', GroupController::class . ':index')->setArgument('access', 'public');
        $this->post('', GroupController::class . ':update')->setArgument('access', 'loggedin');

        $this->post('/enter', GroupController::class . ':enter')->setArgument('access', 'loggedin');
        $this->get('/allocate', GroupController::class . ':allocate')->setArgument('access', 'public');
        $this->get('/partner', GroupController::class . ':getUserPartner')->setArgument('access', 'loggedin');

        $this->group('/people', function() {

            $this->get('', GroupPeopleController::class . ':index')->setArgument('access', 'loggedin');
            $this->get('/reset/{entryId}', GroupPeopleController::class . ':reset')->setArgument('access', 'loggedin');

        });

    });
});

$app->group('/entry', function() {

    $this->group('/{entryId}', function() {

        $this->group('/message', function() {

            $this->get('', MessageController::class . ':index')->setArgument('access', 'loggedin');
            $this->post('', MessageController::class . ':store')->setArgument('access', 'loggedin');

        });

    });

    $this->post('/message-to-santa', MessageController::class . ':store')->setArgument('access', 'loggedin');

});

$app->group('/user', function() {

    $this->get('', UserController::class . ':index')->setArgument('access', 'loggedin');
    $this->get('/groups', GroupController::class . ':userGroups')->setArgument('access', 'loggedin');

    $this->post('/address', UserController::class . ':createAddress')->setArgument('access', 'loggedin');

    $this->get('/messages', MessageController::class . ':indexForUser')->setArgument('access', 'loggedin');

});

$app->post('/login', \Santa\Controllers\User\UserController::class . ':login')->setArgument('access', 'public');

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});