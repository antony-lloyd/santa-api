<?php
namespace Santa\Controllers;

use Slim\Container;
use Slim\Http\Request;

class BaseController
{
	protected $container;
	protected $model;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function getToken(Request $request)
    {
        $token = $request->getHeader('Authorization');
        if (count($token) > 0) {
            return $token[0];
        }
        else {
            return '';
        }
    }

    public function getDecodedToken(Request $request)
    {
        $token = $this->getToken($request);
        if (empty($token)) {
            return [ ];
        }
        else {
            return $this->container->token->verify($token);
        }
    }
}