<?php
namespace Santa\Controllers\Entry;

use Santa\Controllers\BaseController;
use Santa\Models\Entry;
use Santa\Models\EntryMessage;
use Santa\Models\Group;
use Slim\Http\Request;
use Slim\Http\Response;

class MessageController extends BaseController
{
    public function index(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);
        $entry = Entry::with('messages')->find($args['entryId']);

        if ($entry->santa_user_id !== $token->uid) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Unauthorised' ]);
        }

        $parsedMessages = [ ];
        foreach ($entry->messages->reverse() as $message) {
            $parsedMessages[] = $message->exportForUi($token->uid, true);
        }

        return $response->withJson([ 'status' => 'success', 'data' => $parsedMessages ]);
    }

    public function indexForUser(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);
        $entry = Entry::with('messages')->where('user_id', $token->uid)->first();

        $parsedMessages = [ ];
        foreach ($entry->messages->reverse() as $message) {
            $parsedMessages[] = $message->exportForUi($token->uid);
        }

        return $response->withJson([ 'status' => 'success', 'data' => $parsedMessages ]);
    }

    public function store(Request $request, Response $response, array $args)
    {
        $sendingToSanta = strpos($request->getUri(), 'message-to-santa') !== false;

        $token = $this->getDecodedToken($request);

        if ($sendingToSanta) {
            $entry = Entry::where('user_id', $token->uid)->first();
        }
        else {
            $entry = Entry::find($args['entryId']);
        }

        $message = $request->getParam('message');
        if (empty($message)) {
            return $response->withJson([ 'status' => 'fail', 'message' => 'Missing message' ]);
        }

        if ($entry->user_id !== $token->uid && $entry->santa_user_id !== $token->uid) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Unauthorised' ]);
        }

        $model = new EntryMessage();
        $model->user_id = $token->uid;
        $model->message = $message;
        $entry->messages()->save($model);

        return $response->withJson([ 'status' => 'success', 'data' => $model->exportForUi($token->uid), 'message' => 'Message posted' ]);
    }
}