<?php
namespace Santa\Controllers\Group;

use Santa\Controllers\BaseController;
use Santa\Models\Entry;
use Santa\Models\Group;
use Santa\Models\User;
use Slim\Http\Request;
use Slim\Http\Response;

class GroupController extends BaseController
{
    public function index(Request $request, Response $response, array $args)
    {
        return $response->withJson([ 'status' => 'success', 'data' =>  Group::find($args['groupId']) ]);
    }

    public function store(Request $request, Response $response, array $args)
    {
        $name = $request->getParam('name');
        $userName = $request->getParam('user_name');
        $email = $request->getParam('email');
        $password = $request->getParam('password');

        if (empty($name) || empty($userName) || empty($email) || empty($password)) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Missing data' ]);
        }

        // Generate a code
        do {
            $code = substr(str_shuffle(md5(time())), 0, 5);
        } while (Group::where('code', $code)->get()->count() !== 0);

        $group = new Group();
        $group->code = $code;
        $group->name = $name;
        $group->save();

        $user = new User();
        $user->name = $userName;
        $user->email = $email;
        $user->password = password_hash($password, PASSWORD_DEFAULT);
        $user->save();
        $group->users()->attach($user);

        $group->owner_id = $user->id;
        $group->save();

        return $response->withJson([ 'status' => 'success', 'data' => $group ]);
    }

    public function update(Request $request, Response $response, array $args)
    {
        $description = $request->getParam('description');
        $requireAddress = $request->getParam('require_address');

        $group = Group::find($args['groupId']);

        $token = $this->getDecodedToken($request);        
        if ($token->uid !== $group->owner_id) {
            return $response->withJson([ 'status' => 'fail', 'message' => 'Request failed' ]);
        }

        $group->description = $description;
        $group->require_address = $requireAddress;
        $group->save();

        return $response->withJson([ 'status' => 'success', 'data' => $group ]);
    }

    public function join(Request $request, Response $response, array $args)
    {
        $code = $request->getParam('code');
        $name = $request->getParam('name');
        $email = $request->getParam('email');
        $password = $request->getParam('password');
        
        if (empty($code) || empty($name) || empty($email) || empty($password)) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Missing data' ]);
        }

        // Check if user already exists, check the password
        $user = User::where('email', $email);
        if ($user->exists()) {
            $user = $user->first();
            if (!password_verify($password, $user->password)) {
                return $response->withJson([ 'status' => 'fail', 'message' => 'Email already in use. Please provide the correct password.' ]);
            }
        }
        else {
            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->password = password_hash($password, PASSWORD_DEFAULT);
            $user->save();
        }

        // Check if the user is already in the group
        if ($user->groups()->where('code', $code)->count() > 0) {
            return $response->withJson([ 'status' => 'fail', 'message' => 'Already in group' ]);
        }

        $group = Group::where('code', $code)->first();
        $group->users()->attach($user);

        return $response->withJson([ 'status' => 'success', 'data' => $group ]);
    }

    public function userGroups(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);

        $groups = User::find($token->uid)->groups()->get();
        foreach ($groups as &$group) {
            $group['is_owner'] = $group->owner_id === $token->uid;
            $group['entered'] = Entry::where([ [ 'user_id', $token->uid ], [ 'group_id', $group->id ] ])->exists();
        }
        return $response->withJson([ 'status' => 'success', 'data' => $groups, 'message' => 'Fetched groups' ]);
    }

    public function enter(Request $request, Response $response, array $args)
    {
        $hint1 = $request->getParam('hint1');
        $hint2 = $request->getParam('hint2');
        $hint3 = $request->getParam('hint3');
        $notes = $request->getParam('notes');

        if (empty($hint1) || empty($hint2) || empty($hint3)) {
            return [ 'status' => 'error', 'message' => 'Missing data' ];
        }

        $token = $this->getDecodedToken($request);
        $entry = new Entry();
        $entry->user_id = $token->uid;
        $entry->hint1 = $hint1;
        $entry->hint2 = $hint2;
        $entry->hint3 = $hint3;
        $entry->notes = $notes;

        $group = Group::find($args['groupId']);
        $group->entries()->save($entry);

        return $response->withJson([ 'status' => 'success', 'message' => 'Your are entered!' ]);
    }

    public function allocate(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);
        $group = Group::find($args['groupId']);

        if ($group->owner_id !== $token->uid) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Unauthorised' ]);
        }

        $entries = $group->entries->all();
        $availableSantas = $entries;

        foreach ($entries as $entry) {
            while (TRUE) {
                $getCount = (int)(count($availableSantas) / 2);
                $possibleSantas = array_rand($availableSantas, $getCount < 1 ? 1 : $getCount);

                if (!is_array($possibleSantas)) {
                    $santaKey = $possibleSantas;
                }
                else {
                    shuffle($possibleSantas);
                    $santaKey = array_pop($possibleSantas);
                }

                if ($entries[$santaKey]->user_id !== $entry->user_id) {
                    $entry->santa_user_id = $availableSantas[$santaKey]->user_id;
                    $entry->save();
                    unset($availableSantas[$santaKey]);
                    break;
                }
            }
        }
    }

    public function getUserPartner(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);
        return $response->withJson([ 'status' => 'success', 'data' => Entry::with('user.address')->where('santa_user_id', $token->uid)->get(),
            'message' => 'Fetched user partner' ]);
    }
}