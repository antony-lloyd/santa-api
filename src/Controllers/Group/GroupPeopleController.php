<?php
namespace Santa\Controllers\Group;

use Santa\Controllers\BaseController;
use Santa\Models\Entry;
use Santa\Models\Group;
use Santa\Models\User;
use Slim\Http\Request;
use Slim\Http\Response;

class GroupPeopleController extends BaseController
{
    public function index(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);
        $group = Group::find($args['groupId']);

        if ($group->owner_id !== $token->uid) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Unauthorised' ]);
        }

        return $response->withJson([ 'status' => 'success', 'data' => $group->entries()->with('user')->get() ]);
    }

    public function reset(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);
        $group = Group::find($args['groupId']);

        if ($group->owner_id !== $token->uid) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Unauthorised' ]);
        }

        $entry = Entry::find($args['entryId']);
        User::find($entry->user_id)->address()->delete();
        $entry->delete();

        return $response->withJson([ 'status' => 'success', 'message' => 'User entries reset' ]);
    }
}