<?php
namespace Santa\Controllers\User;

use Santa\Models\UserAddress;
use Santa\Controllers\BaseController;
use Santa\Models\User;
use Slim\Http\Request;
use Slim\Http\Response;

class UserController extends BaseController
{
    public function index(Request $request, Response $response, array $args)
    {
        $token = $this->getDecodedToken($request);
        return $response->withJson([ 'status' => 'success', 'data' => User::find($token->uid) ]);
    }

    public function login(Request $request, Response $response, array $args)
    {
        $email = $request->getParam('email');
        $password = $request->getParam('password');

        if (empty($email) || empty($password)) {
            return $response->withJson([ 'status' => 'error', 'message' => 'All fields required' ]);
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Invalid login' ]);
        }

        if (!password_verify($password, $user->password)) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Invalid login' ]);
        }

        $info = [ 'uid' => $user->id ];

        return $response->withJson([ 'status' => 'success',
            'token' => $this->container->token->generate($info),
            'message' => 'Logged in'
        ]);
    }

    public function createAddress(Request $request, Response $response, array $args)
    {
        $addressLine1 = $request->getParam('addressLine1');
        $addressLine2 = $request->getParam('addressLine2');
        $city = $request->getParam('city');
        $postcode = $request->getParam('postcode');
        
        if (empty($addressLine1) || empty($city) || empty($postcode)) {
            return $response->withJson([ 'status' => 'error', 'message' => 'Missing data' ]);
        }

        $address = new UserAddress();
        $address->addressLine1 = $addressLine1;
        $address->addressLine2 = $addressLine2;
        $address->city = $city;
        $address->postcode = $postcode;

        $token = $this->getDecodedToken($request);
        $user = User::find($token->uid);
        $user->address()->save($address);

        return $response->withJson([ 'status' => 'success', 'message' => 'Address added' ]);
    }
}