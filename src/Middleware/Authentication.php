<?php
namespace Santa\Middleware;

class Authentication
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke(\Slim\Http\Request $request, \Slim\Http\Response $response, $next)
    {
        if ($request->getAttribute('route')) {
            $access = $request->getAttribute('route')->getArgument('access');

            if (is_null($access)) {
                $access = 'loggedin';
            }
            else if ($access === 'public') {
               return $next($request, $response);
            }

            $token = $request->getHeader('Authorization')[0];
            if (empty($token)) {
                return $response->withJson([ 'status' => 'fail', 'message' => 'You are not logged in.' ]);
            }

            $data = $this->container->token->verify($token);
            if (!is_object($data)) {
                return $response->withJson([ 'status' => 'error', 'message' => 'Error. You may need to log in again' ]);
            }

            if ($access === 'loggedin') {
                return $next($request, $response);
            }
        }

        $response = $next($request, $response);
        return $response;
    }
}