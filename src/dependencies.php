 <?php
// DIC configuration

$container = $app->getContainer();

$container['token'] = function ($c) {
    return new \Santa\Services\Token($c);
};

$container['mail'] = function ($c) {
  $mail = new PHPMailer\PHPMailer\PHPMailer(true);
  $mail->isSMTP();
  $mail->Host = '';
  $mail->SMTPAuth = true;
  $mail->SMTPKeepAlive = true;
  $mail->Port = 25;
  $mail->Username = '';
  $mail->Password = '';
  $mail->setFrom('noreply@Santa.test', 'Not Reply Santa');
};

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Database
//$container['db'] = function ($c) {
//	$settings = $c->get('settings')['db'];
//
//	$pdo = new \Eats\Helpers\DbWrapper($settings['host'], $settings['name'], $settings['user'], $settings['pass']);
//	return $pdo;
//};

 $capsule = new \Illuminate\Database\Capsule\Manager;
 $capsule->addConnection($container->get('settings')['db']);

 $capsule->setAsGlobal();
 $capsule->bootEloquent();
 $container['db'] = function ($c) use($capsule) {

     return $capsule;
 };